from typing import Any, TypedDict, Literal, Protocol, TypeVar
from os import PathLike
import logging
import io

T = TypeVar("T", Any)

CmdT = TypeVar("CmdT", str)


class AuthT(TypedDict):
    username: str
    password: str


class DevT(TypedDict):
    host: str
    cmds: list[CmdT]
    auth: AuthT


class EachExecInput(TypedDict):
    hostList: list[str]
    targets: dict[str, DevT]


class FgParser:
    def __init__(self, dataSrc: EachExecInput, enable_log: bool = True) -> None:
        self.dataSrc: EachExecInput = dataSrc
        self.main_Log_handler = self._set_log(enable_log)
        self.triggers: dict[str, list[function]] = {
            "beforeAddCmdFromFile": []
        }

    def _set_log(self, enable_log: bool) -> logging.Logger | None:
        """
        :param enable_log:
        :return: logging
        """
        if enable_log:
            main_log_handler = logging.getLogger(__name__)
            main_log_handler.setLevel(logging.INFO)
            log_file_handler = logging.FileHandler(
                filename="exec.log", mode="a")
            log_formatter = logging.Formatter(
                '%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s')
            log_stream_handler = logging.StreamHandler()
            log_file_handler.setFormatter(log_formatter)
            log_stream_handler.setFormatter(log_formatter)
            main_log_handler.addHandler(log_stream_handler)
            main_log_handler.addHandler(log_file_handler)
            return main_log_handler

    def triggerEvent(self, trigerName: str, data: T, **kargs):
        if self.triggers and self.triggers.get(trigerName):
            for f in self.triggers.get(trigerName):
                data: T = f(data)
            return True, data
        return False, data

    def addCmdsFromFile(self, target: str, cmdsFile: str | "PathLike[Any]"):
        if self.dataSrc["targets"].get(target) is None:
            print("target"+target+"not exist.")
            return False
        if self.dataSrc["targets"][target].get("cmds") is None:
            self.dataSrc["targets"][target]["cmds"]=[]
        try:
            with io.open(cmdsFile, "rt", encoding="utf-8") as cfg_file:
                cmds = cfg_file.readlines()
                self.dataSrc["targets"][target]["cmds"] = self.triggerEvent(
                    "beforeAddCmdFromFile", cmds)
                return True
        except Exception:
            print("read file"+cmdsFile+"error occur.")
            return False
