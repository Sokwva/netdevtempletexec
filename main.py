import os,json
import lib


def removeNextLine(r: str):
    return r.replace("....press ENTER to next line, CTRL_C to break, other key to next page....[73D[K", "")


if __name__ == '__main__':
    basePath = os.path.abspath(os.path.dirname(__file__))
    workFilePath = os.path.join(basePath, "test.xlsx")
    x = lib.MultiDevXlsStreamProcess("xls",workFilePath)
    x.pool_semaphere_limit=4
    x.run()
    print(x.fail_list)
