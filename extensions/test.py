from textfsm import TextFSM
import os

baseDir = os.path.abspath(os.path.dirname(__file__))


with open(os.path.join(os.path.join(baseDir, "../logs"), "192.168.1.3.log"), encoding="utf-8") as f:
    raw = f.read()

with open(os.path.join(os.path.join(baseDir, "textfsmTemplate"), "interfaceDetail.textfsm"), encoding="utf-8") as tf:
    template = TextFSM(tf)
    data = template.ParseTextToDicts(raw)
    print(data)
