import logging
import os
import json
import pathlib
from typing import Any, TypedDict, Literal, TypeVar

import netmiko
import netmiko.exceptions
import openpyxl
import openpyxl.worksheet.worksheet
from netmiko import NetmikoTimeoutException
import threading

import textfsm


class ImportedXlsStruct(TypedDict):
    ip: str
    username: str
    password: str
    device_type: str
    session_log: str
    encoding: str
    cfg_file: str


class DeviceExcel:

    def __init__(self, srcType: Literal["xls", "dict"], excel_file, enable_log=True, recordResult=False):
        self.main_Log_handler = self.set_log(enable_log)
        self.result = {}
        self.recordResult = recordResult

        if srcType == "xls":
            self.target_list = self.__reade_excel_to_dicts(excel_file)
        else:
            self.target_list = excel_file

    @staticmethod
    def set_log(enable_log: bool = True):
        """

        :param enable_log:
        :return: logging
        """
        main_log_handler = logging.getLogger(__name__)
        main_log_handler.setLevel(logging.INFO)
        log_file_handler = logging.FileHandler(filename="exec.log", mode="a")
        log_formatter = logging.Formatter(
            '%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s')
        log_stream_handler = logging.StreamHandler()
        log_file_handler.setFormatter(log_formatter)
        log_stream_handler.setFormatter(log_formatter)
        main_log_handler.addHandler(log_stream_handler)
        if enable_log:
            main_log_handler.addHandler(log_file_handler)
        return main_log_handler

    def __reade_excel_to_dicts(self, excel_file) -> list[ImportedXlsStruct]:
        self.main_Log_handler.info("Init Excel Worksheet.")
        ws: openpyxl.worksheet.worksheet.Worksheet = openpyxl.load_workbook(
            excel_file, read_only=True).active
        # print(workFilePath, wb.sheetnames)
        # 行数
        i = 0
        header = []
        target_list = []
        self.main_Log_handler.info("Make targetList.")
        for row in ws.iter_rows():
            exe_target = {}
            # 列数
            n = 0
            for cell in row:
                if i == 0:
                    header.append(cell.value)
                else:
                    if cell.value is None:
                        exe_target[header[n]] = None
                    else:
                        exe_target[header[n]] = cell.value.strip()
                n += 1
            if i == 0:
                i += 1
                continue
            else:
                i += 1
                if exe_target == {}:
                    continue
                target_list.append(exe_target)
        removeBlankTargetList = []
        for i in target_list:
            if i["ip"]:
                removeBlankTargetList.append(i)
        return removeBlankTargetList

    def run(self):
        self.main_Log_handler.info("Make Fail_list.")
        fail_list: list[dict[str, NetmikoTimeoutException | Any]] = []
        for target in self.target_list:
            self.main_Log_handler.info("Make connect_info: " + target["ip"])
            connect_info: ImportedXlsStruct = {
                "ip": target["ip"],
                "username": target["username"],
                "password": target["password"],
                "session_log": target["session_log"],
                "encoding": "utf-8" if target["encoding"] is None else target["encoding"],
                "device_type": target["device_type"],
            }

            if not pathlib.Path(connect_info.get(
                    "session_log")).parent.exists():
                pathlib.Path(connect_info.get(
                    "session_log")).parent.mkdir(parents=True)
                self.main_Log_handler.info("Create session log file")

            try:
                conn = netmiko.ConnectHandler(**connect_info)
                self.main_Log_handler.info("Connecting " + target["ip"])
            except netmiko.exceptions.SSHException as e:
                self.main_Log_handler.warning(
                    "Exec " + target["ip"] + " failed.")
                fail = {
                    "ip": connect_info["ip"],
                    "error_msg": e
                }
                fail_list.append(fail)
                continue
            self.main_Log_handler.info("Exec " + target["ip"])
            conn.send_config_from_file(
                target["cfg_file"], enter_config_mode=False, cmd_verify=False)

    def run_as_save_result(self, result_csv_file):
        result = self.run()
        if len(result) == 0:
            return
        pathlib.Path(result_csv_file).parent.mkdir(parents=True, exist_ok=True)
        import csv
        with open(result_csv_file, mode="w", newline="") as f:
            write = csv.DictWriter(f, fieldnames=result[0].keys())
            write.writeheader()
            write.writerows(result)

        os.startfile(pathlib.Path(result_csv_file))

    def save_result_to_json(self):
        with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "result.json"), "w") as f:
            self.main_Log_handler.info("write result to json file.")
            f.write(json.dumps(self.result, ensure_ascii=False))


class MultiDevExcel(DeviceExcel):
    def __init__(self, srcType: Literal["xls", "dict"], excel_file):
        super().__init__(srcType, excel_file)
        self.fail_list: list[dict[str, NetmikoTimeoutException | Any]] = []
        self.jobs: list[threading.Thread] = []
        self.pool_semaphere_limit: int = 25
        self.pool_semaphereHandler: threading.BoundedSemaphore = threading.BoundedSemaphore(
            value=self.pool_semaphere_limit)
        self.parser: dict[str, list[str]] = {}

    def worker(self, target: ImportedXlsStruct, connect_info: dict):
        with self.pool_semaphereHandler:
            if not pathlib.Path(connect_info.get(
                    "session_log")).parent.exists():
                pathlib.Path(connect_info.get(
                    "session_log")).parent.mkdir(parents=True)
                self.main_Log_handler.info("Create session log file")

            try:
                conn = netmiko.ConnectHandler(**connect_info)
                self.main_Log_handler.info("Connecting " + target["ip"])
            except netmiko.exceptions.SSHException as e:
                self.main_Log_handler.warning(
                    "Exec " + target["ip"] + " failed.")
                fail = {
                    "ip": connect_info["ip"],
                    "error_msg": e
                }
                self.fail_list.append(fail)

            self.main_Log_handler.info("Exec " + target["ip"])
            res = conn.send_config_from_file(target["cfg_file"])
            if self.recordResult:
                self.main_Log_handler.info(f"recordResult {target['ip']} to self.recordResult.")
                self.result[target["ip"]] = res
            self.resultFormater(target,res)

    def resultFormater(self,target:ImportedXlsStruct,res:str):
        if self.parser.get(target["ip"]) is not None:
            self.main_Log_handler.info(f"target {target['ip']} need a formater to be executed.")
            pasers = self.parser.get(target["ip"])
            tempResult = {}
            for f in pasers:
                fser = Formater(os.path.join(os.path.join(os.path.abspath(os.path.dirname(__file__)), "extensions"),
                                                "textfsmTemplate"), f+".textfsm")
                self.main_Log_handler.info(f"create Formater:{f} for target {target['ip']}.")
                fResult = fser.singleRun(res)
                self.main_Log_handler.info(f"executed Formater:{f} for target {target['ip']}.")
                tempResult[f] = fResult
                self.main_Log_handler.info(f"save result Formater:{f} for target {target['ip']}.")
            self.result[target["ip"]] = tempResult

    def run(self):
        self.main_Log_handler.info("Make Fail_list.")
        for target in self.target_list:
            self.main_Log_handler.info("Make connect_info: " + target["ip"])
            connect_info: ImportedXlsStruct = {
                "ip": target["ip"],
                "username": target["username"],
                "password": target["password"],
                "session_log": target["session_log"] if target["session_log"] is not None else "logs/"+target["ip"]+".log",
                "encoding": "utf-8" if target.get("encoding") is None else target["encoding"],
                "device_type": target["device_type"],
            }
            self.jobs.append(threading.Thread(
                target=self.worker, args=(target, connect_info,)))
        for i in self.jobs:
            i.start()
        for j in self.jobs:
            j.join()


class MultiDevXlsStreamProcess(MultiDevExcel):
    def __init__(self, srcType: Literal["xls", "dict"], excel_file):
        super().__init__(srcType, excel_file)
        self.beforeReturn: list[function] = []

    def worker(self, target: ImportedXlsStruct, connect_info: dict):
        with self.pool_semaphereHandler:
            if not pathlib.Path(connect_info.get(
                    "session_log")).parent.exists():
                pathlib.Path(connect_info.get(
                    "session_log")).parent.mkdir(parents=True)
                self.main_Log_handler.info("Create session log file")

            try:
                conn: netmiko.BaseConnection = netmiko.ConnectHandler(
                    **connect_info)
                self.main_Log_handler.info("Connecting " + target["ip"])
            except netmiko.exceptions.SSHException as e:
                self.main_Log_handler.warning(
                    "Exec " + target["ip"] + " failed.")
                fail = {
                    "ip": connect_info["ip"],
                    "error_msg": e
                }
                self.fail_list.append(fail)

            self.main_Log_handler.info("Exec " + target["ip"])
            x = conn.send_config_from_file(
                target["cfg_file"], enter_config_mode=False, cmd_verify=False)
            if not self.beforeReturn:
                self.result[target["ip"]] = x
            else:
                y = ""
                for f in self.beforeReturn:
                    if hasattr(f, "__call__"):
                        self.main_Log_handler.info(
                            "Exec beforeReturn trigger "+getattr(f, "__name__")+" for "+target["ip"]+" command exec response.")
                        y += f(x, target, self)
                self.result[target["ip"]] = y
                self.main_Log_handler.info(
                    "Wrote result of " + target["ip"]+" to Class Instanse.")
            self.resultFormater(target,x)

class Formater:
    def __init__(self, templateRoot: str, templateFileName: str) -> None:
        self.textfsmLibRoot: str = templateRoot
        self.templateFileName: str = templateFileName
        self.result = {}

    def run(self, raw: MultiDevXlsStreamProcess):
        with open(os.path.join(self.textfsmLibRoot, self.templateFileName)) as f:
            template = textfsm.TextFSM(f)
            for h in raw.target_list:
                target = h["ip"]
                self.result[target] = template.ParseTextToDicts(
                    raw.result[target])

    def singleRun(self, rawText: str):
        with open(os.path.join(self.textfsmLibRoot, self.templateFileName)) as f:
            template = textfsm.TextFSM(f)
            return template.ParseTextToDicts(rawText)
