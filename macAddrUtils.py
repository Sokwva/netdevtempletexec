def fourPartToEight(raw: str):
    x = raw.replace("-", "")
    y = []
    e = 0
    for t in x:
        if e % 2 == 0 and e != 0:
            y.append(":")
        y.append(t)
        e += 1
    return "".join(y)

